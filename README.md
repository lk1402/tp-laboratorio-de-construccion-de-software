# TP-Principal Labo-PP1

Trabajo Práctico realizado como parte de un grupo de 5 integrantes para la materia "Laboratorio de Construcción de Software" de la Universidad Nacional de General Sarmiento. El trabajo consistió en desarrollar una aplicación de escritorio que simule las operaciones básicas de una concesionario de autos. La aplicación fue desarrollada en Java y se utilizó MySQL para el manejo de la base de datos. Otras tecnología utilizadas: Eclipse, JUnit, WindowBuilder, Maven, JasperReports, NSIS.
A continuación se agrega un link con un instalador del software para windows 7 junto con el manual  de instalación, el manual de usuarios y un archivo con los nombre de usuarios predeterminados y sus correspondientes contraseñas para tener acceso a las funciones.

https://drive.google.com/file/d/1xjuXMh5jxwyZXPtuNPFJ4hcR0JcHyZod/view?usp=sharing
