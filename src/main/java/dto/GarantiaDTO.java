package dto;

public class GarantiaDTO {
	private int id;
	private String garantia;
	private double precio;

	public GarantiaDTO(int id, String garantia, double precio)
	{
		this.id = id;
		this.garantia = garantia;
		this.precio = precio;
	}
	
	public int getId() 
	{
		return this.id;
	}

	public void setId(int id) 
	{
		this.id = id;
	}

	public String getGarantia() 
	{
		return this.garantia;
	}

	public void setGarantia(String garantia) 
	{
		this.garantia = garantia;
	}
	
	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Override
    public String toString(){
        return this.garantia;
    }
	
//	@Override
//    public boolean equals(Object objeto){
//        if(objeto==null){
//            return false;
//        }else if(((GarantiaDTO)objeto).garantia.equals(this.garantia)&&((GarantiaDTO)objeto).id== (this.id)){
//            return true;
//        }
//        return false;
//    }
}
