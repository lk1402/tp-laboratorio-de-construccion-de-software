package dto;

public class SucursalDTO {
	private int idSucursal; 
	private String nombre;
	private String calle; 
	private String altura; 
	private PaisDTO pais;
	
	public SucursalDTO(int idSucursal,String nombre,String calle,String altura,PaisDTO pais){
		this.idSucursal = idSucursal;
		this.nombre = nombre;
		this.calle = calle;
		this.altura = altura;
		this.pais = pais;
	}

	public int getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(int idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}
	
	@Override
    public String toString(){
        return this.nombre;
    }
	
//------------------------------------------------------------------------------------------------------------------------------------------
	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((SucursalDTO)objeto).nombre.equals(this.nombre)&&((SucursalDTO)objeto).idSucursal == (this.idSucursal)){
            return true;
        }
        return false;
    }

}
