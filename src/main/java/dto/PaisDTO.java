package dto;

public class PaisDTO {
	private int idPais;
	private String pais;

	public PaisDTO(int idPais, String pais)
	{
		this.idPais = idPais;
		this.pais = pais;
	}
	
	public int getIdPais() 
	{
		return this.idPais;
	}

	public void setIdPais(int idPais) 
	{
		this.idPais = idPais;
	}

	public String getPais() 
	{
		return this.pais;
	}

	public void setPais(String pais) 
	{
		this.pais = pais;
	}
	
	@Override
    public String toString(){
        return this.pais;
    }
	
	@Override
    public boolean equals(Object objeto){
        if(objeto==null){
            return false;
        }else if(((PaisDTO)objeto).pais.equals(this.pais)&&((PaisDTO)objeto).idPais == (this.idPais)){
            return true;
        }
        return false;
    }
}
