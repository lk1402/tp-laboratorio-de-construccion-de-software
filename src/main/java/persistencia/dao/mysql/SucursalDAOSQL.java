package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.PaisDTO;
import dto.SucursalDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.SucursalDAO;

public class SucursalDAOSQL implements SucursalDAO{
	private static final String insertSucursal = "INSERT INTO sucursal(id, nombre, calle, altura, pais) VALUES(?, ?, ?, ?, ?)";
	private static final String readSucursalIsExist = "SELECT COUNT(id) as cantidad FROM sucursal WHERE nombre = ?";//cambiado private static final String readSucursalIsExist = "SELECT COUNT(nombre) as cantidad FROM sucursal WHERE id = ?"
	private static final String readSucursalIsExistAndDiferentThisId = "SELECT COUNT(id) as cantidad FROM sucursal WHERE nombre = ? and id != ?";
	private static final String delete = "DELETE FROM sucursal WHERE sucursal.id = ? and sucursal.id != 1";
	private static final String readallSimple = "SELECT sucursal.id as idSucursal, nombre, calle, altura, sucursal.pais, pais.pais as nombrePais FROM sucursal join pais on sucursal.pais = pais.id";
	private static final String readall = "SELECT sucursal.id as idSucursal, nombre, calle, altura, sucursal.pais, pais.pais as nombrePais FROM sucursal join pais on sucursal.pais = pais.id WHERE sucursal.id = ?";
	private static final String updateSucursal = "UPDATE sucursal SET nombre = ?, calle = ?, altura = ? WHERE id = ?";
	private static final String readallMatchNombre = "SELECT sucursal.id as idSucursal, nombre, calle, altura, sucursal.pais, pais.pais as nombrePais FROM sucursal join pais on sucursal.pais = pais.id WHERE nombre like '%' ? '%'";
	private static final String readallMatchCalle = "SELECT sucursal.id as idSucursal, nombre, calle, altura, sucursal.pais, pais.pais as nombrePais FROM sucursal join pais on sucursal.pais = pais.id WHERE calle like '%' ? '%'";
	private static final String readallMatchAltura = "SELECT sucursal.id as idSucursal, nombre, calle, altura, sucursal.pais, pais.pais as nombrePais FROM sucursal join pais on sucursal.pais = pais.id WHERE altura like '%' ? '%'";
	//private static final String readallMatchPais = "SELECT id, nombre, calle, altura, pais FROM sucursal WHERE pais like '%' ? '%'";
	private static final String buscar_x_id = "SELECT sucursal.id as idSucursal, nombre, calle, altura, sucursal.pais, pais.pais as nombrePais FROM sucursal join pais on sucursal.pais = pais.id WHERE sucursal.id = ?";

	public boolean insert(SucursalDTO sucursal){
		PreparedStatement statementSucursal;//sentencia insert sucursal
		PreparedStatement statementIsExistSucursal;//sentencia consulta si sucursal existe
		
		ResultSet resultSetIsExistSucursal;//resultado de si existe la sucursal
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			statementIsExistSucursal = conexion.prepareStatement(readSucursalIsExist);
			statementIsExistSucursal.setString(1, sucursal.getNombre());
			resultSetIsExistSucursal = statementIsExistSucursal.executeQuery();
			if(resultSetIsExistSucursal.next()){
				if(!(resultSetIsExistSucursal.getInt("cantidad")>=1)){ //controlo si existe el cliente, si no existe lo creo
					statementSucursal = conexion.prepareStatement(insertSucursal);
					statementSucursal.setInt(1, sucursal.getIdSucursal());
					statementSucursal.setString(2, sucursal.getNombre());
					statementSucursal.setString(3, sucursal.getCalle());
					statementSucursal.setString(4, sucursal.getAltura());
					statementSucursal.setInt(5, sucursal.getPais().getIdPais());
					if(statementSucursal.executeUpdate() > 0){
						conexion.commit();
						isInsertExitoso = true;
					}else conexion.rollback();
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public boolean update(SucursalDTO sucursalModificada) {
		PreparedStatement statementSucursal;//sentencia update sucursal
		PreparedStatement statementIsExistSucursal;//sentencia consulta si sucursal existe
		
		ResultSet resultSetIsExistSucursal;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try{
			statementIsExistSucursal = conexion.prepareStatement(readSucursalIsExistAndDiferentThisId);
			statementIsExistSucursal.setString(1, sucursalModificada.getNombre());
			/////agregado
			statementIsExistSucursal.setInt(2, sucursalModificada.getIdSucursal());
			//////
			resultSetIsExistSucursal = statementIsExistSucursal.executeQuery();
			if(resultSetIsExistSucursal.next()){
				if(!(resultSetIsExistSucursal.getInt("cantidad")>=1)){
					statementSucursal = conexion.prepareStatement(updateSucursal);
					statementSucursal.setString(1, sucursalModificada.getNombre());
					statementSucursal.setString(2, sucursalModificada.getCalle());
					statementSucursal.setString(3, sucursalModificada.getAltura());
					statementSucursal.setInt(4, sucursalModificada.getIdSucursal());
					if(statementSucursal.executeUpdate() > 0){
						conexion.commit();
						isUpdateExitoso = true;
					}else conexion.rollback();
				}
			}		
			
		}catch(SQLException e) {
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch(SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isUpdateExitoso;
	}  
	
	@Override
	public boolean delete(SucursalDTO sucursal_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(sucursal_a_eliminar.getIdSucursal()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	@Override
	public List<SucursalDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<SucursalDTO> sucursal = new ArrayList<SucursalDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readallSimple);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				sucursal.add(getSucursalDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return sucursal;
	}
	
	@Override
	public List<SucursalDTO> readAllMatchDatSucursal(String dato, int campo) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<SucursalDTO> sucursal = new ArrayList<SucursalDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = null;
			if(campo==1){
				statement = conexion.getSQLConexion().prepareStatement(readallMatchNombre);
			}else if(campo==2){
				statement = conexion.getSQLConexion().prepareStatement(readallMatchCalle);
			}else{
				statement = conexion.getSQLConexion().prepareStatement(readallMatchAltura);
			}//falta filtrar por pais
			statement.setString(1, dato);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				sucursal.add(getSucursalDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return sucursal;
	}
	
	public List<SucursalDTO> readAll(int id)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<SucursalDTO> sucursal = new ArrayList<SucursalDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			statement.setInt(1,id);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				sucursal.add(getSucursalDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return sucursal;
	}

	private SucursalDTO getSucursalDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idSucursal");
		String nombre = resultSet.getString("nombre");
		String calle = resultSet.getString("calle");
		String altura = resultSet.getString("altura");
		int idPais = resultSet.getInt("sucursal.pais");
		String nombrePais = resultSet.getString("nombrePais");
		PaisDTO pais = new PaisDTO(idPais, nombrePais);
		
		
		return new SucursalDTO(id, nombre, calle, altura, pais);
	}

	@Override
	public SucursalDTO buscarPorId(int id) {

		PreparedStatement statement;
		ResultSet resultSet;
		Conexion conexion = Conexion.getConexion();
		SucursalDTO sucursal = null;
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(buscar_x_id);
			statement.setString(1, Integer.toString(id));
			resultSet = statement.executeQuery();
			resultSet.next();
			sucursal  = this.getSucursalDTO(resultSet);
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return sucursal;
	}
	
}
