package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.StockAutomovilDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.PaisDTO;
import dto.SucursalDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.AutomovilDAO;

public class AutomovilDAOSQL implements AutomovilDAO{
	private static final String insertAutomovil = "INSERT INTO autosenstock(id, modelo, year, color, cantidadPuertas, kilometros, stockminimo, stock, sucursal, precio) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";//mod
	private static final String readAutomovilIsExist = "SELECT COUNT(id) as cantidad FROM autosenstock WHERE modelo = ? and year = ? and color = ? and cantidadPuertas = ? and kilometros = ? and sucursal = ?";
	private static final String readIdAndStockAutomovil = "SELECT id, stock, stockminimo FROM autosenstock WHERE modelo = ? and year = ? and color = ? and cantidadPuertas = ? and kilometros = ? and sucursal = ?";
	private static final String readOtherAutomovilIsExist = "SELECT COUNT(id) as cantidad FROM autosenstock WHERE modelo = ? and year = ? and color = ? and cantidadPuertas = ? and kilometros = ? and sucursal = ? and id != ?";	
	private static final String delete = "DELETE FROM autosenstock WHERE id = ?";
	private static final String updateAutomovil = "UPDATE autosenstock SET modelo = ?, year = ?, color = ?, cantidadPuertas = ?, kilometros = ?, stockminimo = ?, stock = ?, sucursal = ?, precio = ? WHERE id = ?";
	private static final String readAllAutomovil = "SELECT autosenstock.id, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, year, color, cantidadPuertas, kilometros, stockminimo, stock, precio, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calleSucursal, sucursal.altura as alturaSucursal, pais.id as idPais, pais.pais as nombrePais FROM autosenstock join modeloauto on autosenstock.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autosenstock.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE autosenstock.modelo = ?";
	private static final String readAllFaltaStock = "SELECT autosenstock.id, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, year, color, cantidadPuertas, kilometros, stockminimo, stock, precio, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calleSucursal, sucursal.altura as alturaSucursal, pais.id as idPais, pais.pais as nombrePais FROM autosenstock join modeloauto on autosenstock.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autosenstock.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE stock < stockminimo";
	private static final String search = "SELECT autosenstock.id, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, year, color, cantidadPuertas, kilometros, stockminimo, stock, precio, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calleSucursal, sucursal.altura as alturaSucursal, pais.id as idPais, pais.pais as nombrePais FROM autosenstock join modeloauto on autosenstock.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autosenstock.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE ";
	private static final String[] arrayCampos = {"modeloauto.id","sucursal.id","year","kilometros"};
	private static final String readAllAutomovilPorSucursal = "SELECT autosenstock.id, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, year, color, cantidadPuertas, kilometros, stockminimo, stock, precio, sucursal.id as idSucursal, sucursal.nombre as nombreSucursal, sucursal.calle as calleSucursal, sucursal.altura as alturaSucursal, pais.id as idPais, pais.pais as nombrePais FROM autosenstock join modeloauto on autosenstock.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join sucursal on autosenstock.sucursal = sucursal.id join pais on sucursal.pais = pais.id WHERE autosenstock.modelo = ? AND autosenstock.sucursal = ?";
	
	
	//	private static final String readAll = "SELECT id, modelo, year, color, cantidadPuertas, stock, sucursal FROM autosenstock";
	/*
	private static final String readallMatchNombre = "SELECT id, nombre, marca, cantPuertas, modelo, patente FROM automovil WHERE nombre like '%' ? '%'";
	private static final String readallMatchMarca = "SELECT id, nombre, marca, cantPuertas, modelo, patente FROM automovil WHERE marca like '%' ? '%'";
	private static final String readallMatchCantPuertas = "SELECT id, nombre, marca, cantPuertas, modelo, patente FROM automovil WHERE cantPuerta like '%' ? '%'";
	private static final String readallMatchModelo = "SELECT id, nombre, marca, cantPuertas, modelo, patente FROM automovil WHERE modelo like '%' ? '%'";
	private static final String readallMatchPatente = "SELECT id, nombre, marca, cantPuertas, modelo, patente FROM automovil WHERE patente like '%' ? '%'";
	*/
	
	@Override
	public boolean insert(StockAutomovilDTO automovil) {
		PreparedStatement statementAutomovil;
		PreparedStatement statementIsExistAutomovil;
		PreparedStatement statementStockAutomovil;
		
		ResultSet resultSetIsExistSucursal;
		ResultSet resultSetStockAutomovil;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			statementIsExistAutomovil = conexion.prepareStatement(readAutomovilIsExist);
			statementIsExistAutomovil.setInt(1,automovil.getModelo().getId());
			statementIsExistAutomovil.setString(2,automovil.getYear());
			statementIsExistAutomovil.setString(3,automovil.getColor());
			statementIsExistAutomovil.setString(4,automovil.getCantidadPuertas());
			statementIsExistAutomovil.setInt(5,automovil.getKilometraje());//agregado
			statementIsExistAutomovil.setInt(6,automovil.getSucursal().getIdSucursal());
			resultSetIsExistSucursal = statementIsExistAutomovil.executeQuery();
			
			if(resultSetIsExistSucursal.next()){
				if(!(resultSetIsExistSucursal.getInt("cantidad")>=1)){
					statementAutomovil = conexion.prepareStatement(insertAutomovil);
					statementAutomovil.setInt(1, automovil.getId());
					statementAutomovil.setInt(2, automovil.getModelo().getId());
					statementAutomovil.setString(3, automovil.getYear());
					statementAutomovil.setString(4, automovil.getColor());
					statementAutomovil.setString(5, automovil.getCantidadPuertas());
					statementAutomovil.setInt(6,automovil.getKilometraje());//agregado
					statementAutomovil.setInt(7, automovil.getStockMinimo());
					statementAutomovil.setInt(8, automovil.getStock());
					statementAutomovil.setInt(9, automovil.getSucursal().getIdSucursal());
					statementAutomovil.setDouble(10, automovil.getPrecio());

					if(statementAutomovil.executeUpdate() > 0){
						conexion.commit();
						isInsertExitoso = true;
					}else conexion.rollback();
				}else{
					
					statementStockAutomovil = conexion.prepareStatement(readIdAndStockAutomovil);
					statementStockAutomovil.setInt(1,automovil.getModelo().getId());
					statementStockAutomovil.setString(2,automovil.getYear());
					statementStockAutomovil.setString(3,automovil.getColor());
					statementStockAutomovil.setString(4,automovil.getCantidadPuertas());
					statementStockAutomovil.setInt(5,automovil.getKilometraje());//agregado
					statementStockAutomovil.setInt(6,automovil.getSucursal().getIdSucursal());
					resultSetStockAutomovil = statementStockAutomovil.executeQuery();
						
					if(resultSetStockAutomovil.next()){
						int stock = resultSetStockAutomovil.getInt("stock");
						int stockminimo = resultSetStockAutomovil.getInt("stockminimo");
						int nuevoStock = (automovil.getStock());
						nuevoStock = stock + nuevoStock;
						statementAutomovil = conexion.prepareStatement(updateAutomovil);
						statementAutomovil.setInt(1, automovil.getModelo().getId());
						statementAutomovil.setString(2, automovil.getYear());
						statementAutomovil.setString(3, automovil.getColor());
						statementAutomovil.setString(4, automovil.getCantidadPuertas());
						statementAutomovil.setInt(5,automovil.getKilometraje());//agregado
						statementAutomovil.setInt(6, stockminimo);
						statementAutomovil.setInt(7, nuevoStock);
						statementAutomovil.setInt(8, automovil.getSucursal().getIdSucursal());
						statementAutomovil.setDouble(9, automovil.getPrecio());
						statementAutomovil.setInt(10, resultSetStockAutomovil.getInt("id"));
							
						if(statementAutomovil.executeUpdate() > 0)
						{
							conexion.commit();
							isInsertExitoso = true;
						}
						else conexion.rollback();
						
					}
				}
			}
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}


	@Override
	public boolean delete(StockAutomovilDTO automovil_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setInt(1, automovil_a_eliminar.getId());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return isdeleteExitoso;

	}

	@Override
	public boolean update(StockAutomovilDTO automovilModificado) {
		PreparedStatement statementAutomovil;
		PreparedStatement statementIsExistAutomovil;
		
		ResultSet resultSetIsExistSucursal;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isEditarExitoso = false;
		
		try{		
			statementIsExistAutomovil = conexion.prepareStatement(readOtherAutomovilIsExist);
			statementIsExistAutomovil.setInt(1,automovilModificado.getModelo().getId());
			statementIsExistAutomovil.setString(2,automovilModificado.getYear());
			statementIsExistAutomovil.setString(3,automovilModificado.getColor());
			statementIsExistAutomovil.setString(4,automovilModificado.getCantidadPuertas());
			statementIsExistAutomovil.setInt(5,automovilModificado.getKilometraje());//agregado
			statementIsExistAutomovil.setInt(6,automovilModificado.getSucursal().getIdSucursal());
			statementIsExistAutomovil.setInt(7,automovilModificado.getId());
			resultSetIsExistSucursal = statementIsExistAutomovil.executeQuery();
			
			if(resultSetIsExistSucursal.next()){
				if(!(resultSetIsExistSucursal.getInt("cantidad")>=1)){
					statementAutomovil = conexion.prepareStatement(updateAutomovil);
					statementAutomovil.setInt(1, automovilModificado.getModelo().getId());
					statementAutomovil.setString(2, automovilModificado.getYear());
					statementAutomovil.setString(3, automovilModificado.getColor());
					statementAutomovil.setString(4, automovilModificado.getCantidadPuertas());
					statementAutomovil.setInt(5,automovilModificado.getKilometraje());//agregado
					statementAutomovil.setInt(6, automovilModificado.getStockMinimo());
					statementAutomovil.setInt(7, automovilModificado.getStock());
					statementAutomovil.setInt(8, automovilModificado.getSucursal().getIdSucursal());
					statementAutomovil.setDouble(9, automovilModificado.getPrecio());
					statementAutomovil.setInt(10, automovilModificado.getId());

					if(statementAutomovil.executeUpdate() > 0)
					{
						conexion.commit();
						isEditarExitoso = true;
					}
					else conexion.rollback();
				}
			}
					
			
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isEditarExitoso;
	}	

	@Override
	public List<StockAutomovilDTO> readConModelos(int idModelo) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<StockAutomovilDTO> automovil = new ArrayList<StockAutomovilDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAllAutomovil);
			statement.setInt(1,idModelo);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				automovil.add(getAutomovilDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return automovil;
	}
	
	@Override
	public List<StockAutomovilDTO> readConModelosPorSucursal(int idModelo, int idSucursal) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<StockAutomovilDTO> automovil = new ArrayList<StockAutomovilDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAllAutomovilPorSucursal);
			statement.setInt(1,idModelo);
			statement.setInt(2,idSucursal);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				automovil.add(getAutomovilDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return automovil;
	}
	
//	public List<StockAutomovilDTO> readAll()
//	{
//		PreparedStatement statement;
//		ResultSet resultSet; //Guarda el resultado de la query
//		ArrayList<StockAutomovilDTO> automoviles = new ArrayList<StockAutomovilDTO>();
//		Conexion conexion = Conexion.getConexion();
//		try{
//			statement = conexion.getSQLConexion().prepareStatement(readAll);
//			resultSet = statement.executeQuery();
//			while(resultSet.next()){
//				automoviles.add(getAutomovilDTO(resultSet));
//			}
//		}catch (SQLException e){
//			e.printStackTrace();
//		}
//		return automoviles;
//	}
	
	private StockAutomovilDTO getAutomovilDTO(ResultSet resultSet) throws SQLException{
		
		int id = resultSet.getInt("id");
		int idMarca = resultSet.getInt("idMarca");
		String nombreMarca = resultSet.getString("nombreMarca");
		MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
		int idModelo = resultSet.getInt("idModelo");
		String nombreModelo = resultSet.getString("nombreModelo");
		ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
		String year = resultSet.getString("year");
		String color = resultSet.getString("color");
		String cantidadPuertas = resultSet.getString("cantidadPuertas");
		int kilometros = resultSet.getInt("kilometros");//agregado
		int stock = resultSet.getInt("stock");
		int stockMinimo = resultSet.getInt("stockminimo");
		int idSucursal = resultSet.getInt("idSucursal");
		String nombreSucursal = resultSet.getString("nombreSucursal");
		String calleSucursal = resultSet.getString("calleSucursal");
		String alturaSucursal = resultSet.getString("alturaSucursal");
		int idPais = resultSet.getInt("idPais");
		String nombrePais = resultSet.getString("nombrePais");
		PaisDTO pais = new PaisDTO(idPais, nombrePais);
		SucursalDTO sucursal = new SucursalDTO(idSucursal, nombreSucursal, calleSucursal, alturaSucursal, pais);
		double precio = resultSet.getDouble("precio"); 
		
		return new StockAutomovilDTO(id, modelo, year, color, cantidadPuertas, kilometros, stockMinimo, stock, sucursal, precio);
	}


	@Override
	public List<StockAutomovilDTO> readAllFaltaStock() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<StockAutomovilDTO> automovil = new ArrayList<StockAutomovilDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readAllFaltaStock);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				automovil.add(getAutomovilDTO(resultSet));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return automovil;
	}


	@Override
	public List<StockAutomovilDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda) {
	
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<StockAutomovilDTO> automoviles = new ArrayList<StockAutomovilDTO>();
		Conexion conexion = Conexion.getConexion();
		
		try {
			String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(this.search, this.arrayCampos, filtrosBusqueda);
			if(queryBusqueda!=null) {
				statement = conexion.getSQLConexion().prepareStatement(queryBusqueda);
				for(int i=0;i<filtrosBusqueda.size();i++) {
					statement.setString(i+1,filtrosBusqueda.get(i).getRight());
				}
				resultSet = statement.executeQuery();
				while(resultSet.next()) {
					automoviles.add(getAutomovilDTO(resultSet));
				}
				
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		return automoviles;
	}

	
}
