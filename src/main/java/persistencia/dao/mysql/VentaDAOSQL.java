package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutoparteDTO;
import dto.ClienteDTO;
import dto.EstadoVentaDTO;
import dto.GarantiaDTO;
import dto.MantenimientoDTO;
import dto.MedioPagoDTO;
import dto.OpcionPagoDTO;
import dto.StockAutomovilDTO;
import dto.SucursalDTO;
import dto.TarjetaDTO;
import dto.TipoDocumentoDTO;
import dto.UsuarioDTO;
import dto.VentaDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.VentaDAO;

public class VentaDAOSQL implements VentaDAO{
	private static final String insertVentaSinTarjeta = "INSERT INTO venta(id, fecha, cliente, opcionpago, montoApagar, estado, sucursal, usuario) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String insertVentaConTarjeta = "INSERT INTO venta(id, fecha, cliente, opcionpago, montoApagar, tarjeta, estado, sucursal, usuario) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String insertAutopartesxVenta = "INSERT INTO autopartesxventas(id, autoparte, venta) VALUES(?, ?, ?)";
	private static final String insertAutomovilesxVenta = "INSERT INTO autosxventas(id, auto, venta, garantia) VALUES(?, ?, ?, ?)";
	private static final String insertAutomovilesxVentaSinGarantia = "INSERT INTO autosxventas(id, auto, venta) VALUES(?, ?, ?)";
	private static final String insertTarjeta = "INSERT INTO tarjeta(id, numeroTarjeta, codigoSeguridad, nombreTitular, vencimiento) VALUES(?, ?, ?, ?, ?)";
	private static final String readLastTarjeta = "SELECT MAX(id) as id From tarjeta";
	private static final String readLastVenta = "SELECT MAX(id) as id From venta";
	private static final String readStockVehiculos = "SELECT stock From autosenstock WHERE id = ?";
	private static final String readLastStockAutoparte = "SELECT stock From autoparte WHERE idautoparte = ?";
	private static final String updateStockVehiculos = "UPDATE autosenstock SET stock = ? WHERE id = ?";
	private static final String updateStockAutopartes = "UPDATE autoparte SET stock = ? WHERE idautoparte = ?";
	private static final String updateVentaMantenimiento = "UPDATE mantenimiento SET venta = ?, finalizado = ? WHERE id = ?";
	private static final String readall = "SELECT v.id AS idVenta, v.fecha AS fechaVenta, c.id AS idCliente, c.nombre AS nombreCliente, c.apellido AS apellidoCliente, td.id AS idTipoDocumentoCliente, td.tipoDocumento AS tipoDocumentoCliente, c.numdocumento AS numDocumentoCliente, op.id AS idOpcionPago, op.opcion AS opcionPago, mp.id AS idMedioPago, mp.nombre AS medioPago, v.montoApagar AS monto, t.id AS idTarjeta, t.numeroTarjeta  AS nroTarjeta, v.estado AS estado, s.id AS idSucursal, s.nombre AS nombreSucursal, ev.id AS idEstadoVenta, ev.estado AS estadoVenta FROM venta AS v JOIN cliente AS c ON v.cliente = c.id JOIN tipodocumento AS td ON c.tipoDocumento = td.id JOIN opcionpago AS op ON v.opcionpago = op.id JOIN mediospago AS mp ON op.medioPago = mp.id LEFT JOIN tarjeta AS t ON v.tarjeta = t.id JOIN sucursal AS s ON v.sucursal = s.id JOIN estadoventa AS ev ON v.estado = ev.id";
	private static final String updateEstadoVenta = "UPDATE venta SET estado = ? WHERE id = ?";
	private static final String search = "SELECT v.id AS idVenta, v.fecha AS fechaVenta, c.id AS idCliente, c.nombre AS nombreCliente, c.apellido AS apellidoCliente, td.id AS idTipoDocumentoCliente, td.tipoDocumento AS tipoDocumentoCliente, c.numdocumento AS numDocumentoCliente, op.id AS idOpcionPago, op.opcion AS opcionPago, mp.id AS idMedioPago, mp.nombre AS medioPago, v.montoApagar AS monto, t.id AS idTarjeta, t.numeroTarjeta  AS nroTarjeta, v.estado AS estado, s.id AS idSucursal, s.nombre AS nombreSucursal, ev.id AS idEstadoVenta, ev.estado AS estadoVenta FROM venta AS v JOIN cliente AS c ON v.cliente = c.id JOIN tipodocumento AS td ON c.tipoDocumento = td.id JOIN opcionpago AS op ON v.opcionpago = op.id JOIN mediospago AS mp ON op.medioPago = mp.id LEFT JOIN tarjeta AS t ON v.tarjeta = t.id JOIN sucursal AS s ON v.sucursal = s.id JOIN estadoventa AS ev ON v.estado = ev.id WHERE ";
	private static final String[] arrayCampos = {"c.nombre","c.apellido","c.numdocumento","v.fecha","ev.estado","s.nombre"};
	
	
	@Override
	public boolean insert(VentaDTO nuevaVenta) {
		PreparedStatement statementVenta;//sentencia insert marca
		PreparedStatement statementautoxventa;
		PreparedStatement statementautopartesxventa;
		PreparedStatement statementUltimaVenta;
		PreparedStatement statementTarjeta;
		PreparedStatement statementUltimaTarjeta;
		PreparedStatement statementActualizarStockAutomoviles;
		PreparedStatement statementActualizarStockAutopartes;
		PreparedStatement statementStockVehiculos;
		PreparedStatement statementStockAutoparte;
		
		ResultSet resultSetUltimaTarjeta;
		ResultSet resultSetUltimaVenta;
		ResultSet resultSetStockVehiculos;
		ResultSet resultSetStockAutoparte;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			int idTarjeta = 0;
			if(nuevaVenta.getTarjeta()!=null) {
				statementTarjeta = conexion.prepareStatement(insertTarjeta);
				statementTarjeta.setInt(1, 0);
				statementTarjeta.setString(2, nuevaVenta.getTarjeta().getNumero());
				statementTarjeta.setString(3, nuevaVenta.getTarjeta().getCodigo());
				statementTarjeta.setString(4, nuevaVenta.getTarjeta().getTitular());
				statementTarjeta.setString(5, nuevaVenta.getTarjeta().getVencimiento());
				if(statementTarjeta.executeUpdate() > 0){
					statementUltimaTarjeta = conexion.prepareStatement(readLastTarjeta);
					resultSetUltimaTarjeta = statementUltimaTarjeta.executeQuery();
					if(resultSetUltimaTarjeta.next()) {
						idTarjeta = resultSetUltimaTarjeta.getInt("id");
						statementVenta = conexion.prepareStatement(insertVentaConTarjeta);
						statementVenta.setInt(1, nuevaVenta.getId());
						statementVenta.setString(2, nuevaVenta.getFecha());
						statementVenta.setInt(3, nuevaVenta.getCliente().getIdCliente());
						statementVenta.setInt(4, nuevaVenta.getOpcionPago().getId());
						statementVenta.setDouble(5, nuevaVenta.getMonto());
						statementVenta.setInt(6, idTarjeta);
						statementVenta.setInt(7, nuevaVenta.getEstado().getIdEstado());
						statementVenta.setInt(8, nuevaVenta.getSucursal().getIdSucursal());
						statementVenta.setInt(9, nuevaVenta.getIdUsr());
						
						if(statementVenta.executeUpdate() > 0){
							statementUltimaVenta = conexion.prepareStatement(readLastVenta);
							resultSetUltimaVenta = statementUltimaVenta.executeQuery();
							if(resultSetUltimaVenta.next()) {
								int idVenta = resultSetUltimaVenta.getInt("id");
								boolean sinError = true;
								if(nuevaVenta.getAutomoviles()!=null && nuevaVenta.getAutomoviles().size()>0) {
									for(Pair<StockAutomovilDTO, GarantiaDTO> auto : nuevaVenta.getAutomoviles()) {
										if(auto.getRight()==null) { 
										statementautoxventa = conexion.prepareStatement(insertAutomovilesxVentaSinGarantia);
										statementautoxventa.setInt(1, 0);
										statementautoxventa.setInt(2, auto.getLeft().getId());
										statementautoxventa.setInt(3, idVenta);
										} else {
											statementautoxventa = conexion.prepareStatement(insertAutomovilesxVenta);
											statementautoxventa.setInt(1, 0);
											statementautoxventa.setInt(2, auto.getLeft().getId());
											statementautoxventa.setInt(3, idVenta);
											statementautoxventa.setInt(4, auto.getRight().getId());

										}
										sinError = sinError && (statementautoxventa.executeUpdate()>0);
										
										statementStockVehiculos = conexion.prepareStatement(readStockVehiculos);
										statementStockVehiculos.setInt(1, auto.getLeft().getId());
										resultSetStockVehiculos = statementStockVehiculos.executeQuery();
										if(resultSetStockVehiculos.next()){
											int stockActual = resultSetStockVehiculos.getInt("stock");
											statementActualizarStockAutomoviles = conexion.prepareStatement(updateStockVehiculos);
											statementActualizarStockAutomoviles.setInt(1, (stockActual-1));
											statementActualizarStockAutomoviles.setInt(2, auto.getLeft().getId());
											sinError = sinError && (statementActualizarStockAutomoviles.executeUpdate()>0);
										}else{
											sinError = false;
										}									
									}
								}
								if(nuevaVenta.getAutopartes()!=null && nuevaVenta.getAutopartes().size()>0) {
									for(AutoparteDTO autoparte : nuevaVenta.getAutopartes()) {
										statementautopartesxventa = conexion.prepareStatement(insertAutopartesxVenta);
										statementautopartesxventa.setInt(1, 0);
										statementautopartesxventa.setInt(2, autoparte.getId());
										statementautopartesxventa.setInt(3, idVenta);
										sinError = sinError && (statementautopartesxventa.executeUpdate()>0);
										
										
										statementStockAutoparte = conexion.prepareStatement(readLastStockAutoparte);
										statementStockAutoparte.setInt(1, autoparte.getId());
										resultSetStockAutoparte = statementStockAutoparte.executeQuery();
										if(resultSetStockAutoparte.next()){
											int stockActual = resultSetStockAutoparte.getInt("stock");
											statementActualizarStockAutopartes = conexion.prepareStatement(updateStockAutopartes);
											statementActualizarStockAutopartes.setInt(1, (stockActual-1));
											statementActualizarStockAutopartes.setInt(2, autoparte.getId());
											sinError = sinError && (statementActualizarStockAutopartes.executeUpdate()>0);
										}else{
											sinError = false;
										}
									}
								}
								if(sinError) {
									conexion.commit();
									isInsertExitoso = true;
								}else conexion.rollback();
							}else conexion.rollback();
						}else conexion.rollback();
						
					}else conexion.rollback();
				}else conexion.rollback();
			}else {
				statementVenta = conexion.prepareStatement(insertVentaSinTarjeta);
				statementVenta.setInt(1, nuevaVenta.getId());
				statementVenta.setString(2, nuevaVenta.getFecha());
				statementVenta.setInt(3, nuevaVenta.getCliente().getIdCliente());
				statementVenta.setInt(4, nuevaVenta.getOpcionPago().getId());
				statementVenta.setDouble(5, nuevaVenta.getMonto());
				statementVenta.setInt(6, nuevaVenta.getEstado().getIdEstado());
				statementVenta.setInt(7, nuevaVenta.getSucursal().getIdSucursal());
				statementVenta.setInt(8, nuevaVenta.getIdUsr());
				
				
				if(statementVenta.executeUpdate() > 0){
					statementUltimaVenta = conexion.prepareStatement(readLastVenta);
					resultSetUltimaVenta = statementUltimaVenta.executeQuery();
					if(resultSetUltimaVenta.next()) {
						int idVenta = resultSetUltimaVenta.getInt("id");
						boolean sinError = true;
						if(nuevaVenta.getAutomoviles()!=null && nuevaVenta.getAutomoviles().size()>0) {
							for(Pair<StockAutomovilDTO, GarantiaDTO> auto : nuevaVenta.getAutomoviles()) {
								if(auto.getRight()==null) {
								statementautoxventa = conexion.prepareStatement(insertAutomovilesxVentaSinGarantia);
								statementautoxventa.setInt(1, 0);
								statementautoxventa.setInt(2, auto.getLeft().getId());
								statementautoxventa.setInt(3, idVenta);
								} else {
									statementautoxventa = conexion.prepareStatement(insertAutomovilesxVenta);
									statementautoxventa.setInt(1, 0);
									statementautoxventa.setInt(2, auto.getLeft().getId());
									statementautoxventa.setInt(3, idVenta);
									statementautoxventa.setInt(4, auto.getRight().getId());		
								}
								sinError = sinError && (statementautoxventa.executeUpdate()>0);
								
								statementStockVehiculos = conexion.prepareStatement(readStockVehiculos);
								statementStockVehiculos.setInt(1, auto.getLeft().getId());
								resultSetStockVehiculos = statementStockVehiculos.executeQuery();
								if(resultSetStockVehiculos.next()){
									int stockActual = resultSetStockVehiculos.getInt("stock");
									statementActualizarStockAutomoviles = conexion.prepareStatement(updateStockVehiculos);
									statementActualizarStockAutomoviles.setInt(1, (stockActual-1));
									statementActualizarStockAutomoviles.setInt(2, auto.getLeft().getId());
									sinError = sinError && (statementActualizarStockAutomoviles.executeUpdate()>0);
								}else{
									sinError = false;
								}
								
							}
						}
						if(nuevaVenta.getAutopartes()!=null && nuevaVenta.getAutopartes().size()>0) {
							for(AutoparteDTO autoparte : nuevaVenta.getAutopartes()) {
								statementautopartesxventa = conexion.prepareStatement(insertAutopartesxVenta);
								statementautopartesxventa.setInt(1, 0);
								statementautopartesxventa.setInt(2, autoparte.getId());
								statementautopartesxventa.setInt(3, idVenta);
								sinError = sinError && (statementautopartesxventa.executeUpdate()>0);
								
								statementStockAutoparte = conexion.prepareStatement(readLastStockAutoparte);
								statementStockAutoparte.setInt(1, autoparte.getId());
								resultSetStockAutoparte = statementStockAutoparte.executeQuery();
								if(resultSetStockAutoparte.next()){
									int stockActual = resultSetStockAutoparte.getInt("stock");
									statementActualizarStockAutopartes = conexion.prepareStatement(updateStockAutopartes);
									statementActualizarStockAutopartes.setInt(1, (stockActual-1));
									statementActualizarStockAutopartes.setInt(2, autoparte.getId());
									sinError = sinError && (statementActualizarStockAutopartes.executeUpdate()>0);
								}else{
									sinError = false;
								}
								
							}
						}
						if(sinError && ((nuevaVenta.getAutomoviles().size()>0) || (nuevaVenta.getAutopartes().size()>0))) {
							conexion.commit();
							isInsertExitoso = true;
						}else conexion.rollback();
					}else conexion.rollback();
				}else conexion.rollback();
			}		
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	public boolean insertVentaMantenimiento(VentaDTO nuevaVenta, MantenimientoDTO mantenimiento) {
		PreparedStatement statementVenta;//sentencia insert marca
		PreparedStatement statementautoxventa;
		PreparedStatement statementautopartesxventa;
		PreparedStatement statementUltimaVenta;
		PreparedStatement statementTarjeta;
		PreparedStatement statementUltimaTarjeta;
		PreparedStatement statementActualizarStockAutomoviles;
		PreparedStatement statementActualizarStockAutopartes;
		PreparedStatement statementStockVehiculos;
		PreparedStatement statementStockAutoparte;
		PreparedStatement statementVentaMantenimiento;		
		
		ResultSet resultSetUltimaTarjeta;
		ResultSet resultSetUltimaVenta;
		ResultSet resultSetStockVehiculos;
		ResultSet resultSetStockAutoparte;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			int idTarjeta = 0;
			if(nuevaVenta.getTarjeta()!=null) {
				statementTarjeta = conexion.prepareStatement(insertTarjeta);
				statementTarjeta.setInt(1, 0);
				statementTarjeta.setString(2, nuevaVenta.getTarjeta().getNumero());
				statementTarjeta.setString(3, nuevaVenta.getTarjeta().getCodigo());
				statementTarjeta.setString(4, nuevaVenta.getTarjeta().getTitular());
				statementTarjeta.setString(5, nuevaVenta.getTarjeta().getVencimiento());
				if(statementTarjeta.executeUpdate() > 0){
					statementUltimaTarjeta = conexion.prepareStatement(readLastTarjeta);
					resultSetUltimaTarjeta = statementUltimaTarjeta.executeQuery();
					if(resultSetUltimaTarjeta.next()) {
						idTarjeta = resultSetUltimaTarjeta.getInt("id");
						statementVenta = conexion.prepareStatement(insertVentaConTarjeta);
						statementVenta.setInt(1, nuevaVenta.getId());
						statementVenta.setString(2, nuevaVenta.getFecha());
						statementVenta.setInt(3, nuevaVenta.getCliente().getIdCliente());
						statementVenta.setInt(4, nuevaVenta.getOpcionPago().getId());
						statementVenta.setDouble(5, nuevaVenta.getMonto());
						statementVenta.setInt(6, idTarjeta);
						statementVenta.setInt(7, nuevaVenta.getEstado().getIdEstado());
						statementVenta.setInt(8, nuevaVenta.getSucursal().getIdSucursal());
						statementVenta.setInt(9, nuevaVenta.getIdUsr());
						
						if(statementVenta.executeUpdate() > 0){
							statementUltimaVenta = conexion.prepareStatement(readLastVenta);
							resultSetUltimaVenta = statementUltimaVenta.executeQuery();
							if(resultSetUltimaVenta.next()) {
								int idVenta = resultSetUltimaVenta.getInt("id");
								boolean sinError = true;
								if(nuevaVenta.getAutopartes()!=null && nuevaVenta.getAutopartes().size()>0) {
									for(AutoparteDTO autoparte : nuevaVenta.getAutopartes()) {
										statementautopartesxventa = conexion.prepareStatement(insertAutopartesxVenta);
										statementautopartesxventa.setInt(1, 0);
										statementautopartesxventa.setInt(2, autoparte.getId());
										statementautopartesxventa.setInt(3, idVenta);
										sinError = sinError && (statementautopartesxventa.executeUpdate()>0);
									}
								}
								if(sinError && ((nuevaVenta.getAutomoviles().size()>0) || (nuevaVenta.getAutopartes().size()>0))) {
									statementVentaMantenimiento = conexion.prepareStatement(updateVentaMantenimiento);
									statementVentaMantenimiento.setInt(1, idVenta);
									statementVentaMantenimiento.setBoolean(2, true);
									statementVentaMantenimiento.setInt(3, mantenimiento.getId());
									if(statementVentaMantenimiento.executeUpdate() > 0){
										conexion.commit();
										isInsertExitoso = true;
									}else conexion.rollback();
								}else conexion.rollback();
							}else conexion.rollback();
						}else conexion.rollback();
						
					}else conexion.rollback();
				}else conexion.rollback();
			}else {
				statementVenta = conexion.prepareStatement(insertVentaSinTarjeta);
				statementVenta.setInt(1, nuevaVenta.getId());
				statementVenta.setString(2, nuevaVenta.getFecha());
				statementVenta.setInt(3, nuevaVenta.getCliente().getIdCliente());
				statementVenta.setInt(4, nuevaVenta.getOpcionPago().getId());
				statementVenta.setDouble(5, nuevaVenta.getMonto());
				statementVenta.setInt(6, nuevaVenta.getEstado().getIdEstado());
				statementVenta.setInt(7, nuevaVenta.getSucursal().getIdSucursal());
				statementVenta.setInt(8, nuevaVenta.getIdUsr());
				
				
				if(statementVenta.executeUpdate() > 0){
					statementUltimaVenta = conexion.prepareStatement(readLastVenta);
					resultSetUltimaVenta = statementUltimaVenta.executeQuery();
					if(resultSetUltimaVenta.next()) {
						int idVenta = resultSetUltimaVenta.getInt("id");
						boolean sinError = true;
						if(nuevaVenta.getAutopartes()!=null && nuevaVenta.getAutopartes().size()>0) {
							for(AutoparteDTO autoparte : nuevaVenta.getAutopartes()) {
								statementautopartesxventa = conexion.prepareStatement(insertAutopartesxVenta);
								statementautopartesxventa.setInt(1, 0);
								statementautopartesxventa.setInt(2, autoparte.getId());
								statementautopartesxventa.setInt(3, idVenta);
								sinError = sinError && (statementautopartesxventa.executeUpdate()>0);			
							}
						}
						if(sinError && ((nuevaVenta.getAutomoviles().size()>0) || (nuevaVenta.getAutopartes().size()>0))) {
							statementVentaMantenimiento = conexion.prepareStatement(updateVentaMantenimiento);
							statementVentaMantenimiento.setInt(1, idVenta);
							statementVentaMantenimiento.setBoolean(2, true);
							statementVentaMantenimiento.setInt(3, mantenimiento.getId());
							if(statementVentaMantenimiento.executeUpdate() > 0){
								conexion.commit();
								isInsertExitoso = true;
							}else conexion.rollback();
						}else conexion.rollback();
					}else conexion.rollback();
				}else conexion.rollback();
			}		
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	@Override
	public List<VentaDTO> readAll() {
		
		PreparedStatement statement;
		ResultSet resultSet;
		ArrayList<VentaDTO> ventas = new ArrayList<VentaDTO>();
		Conexion conexion = Conexion.getConexion();
		
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ventas.add(getVentaDTO(resultSet));
			}
	}catch (SQLException e){
		e.printStackTrace();
	}
		return ventas;

	}

	private VentaDTO getVentaDTO(ResultSet resultSet) throws SQLException{
			int idVenta = resultSet.getInt("idVenta");
		
			String fecha = resultSet.getString("fechaVenta");
		
			int idCliente = resultSet.getInt("idCliente");
			String nombre = resultSet.getString("nombreCliente");
			String apellido = resultSet.getString("apellidoCliente");
			String numDocumento = resultSet.getString("numDocumentoCliente");
		
			int idTipoDoc = resultSet.getInt("idTipoDocumentoCliente");
			String tipoDoc = resultSet.getString("tipoDocumentoCliente");
			
			TipoDocumentoDTO tipoDocumento = new TipoDocumentoDTO(idTipoDoc,tipoDoc);
			ClienteDTO cliente = new ClienteDTO(idCliente,nombre,apellido,null,tipoDocumento,numDocumento,null,null);
			
			int idOpcionPago = resultSet.getInt("idOpcionPago");
			String nombreOpcion = resultSet.getString("opcionPago");
			
			int idMedioPago = resultSet.getInt("idMedioPago");
			String nombreMedio  = resultSet.getString("medioPago");
			
			MedioPagoDTO medioPago = new MedioPagoDTO(idMedioPago,nombreMedio);
			OpcionPagoDTO opcionPago = new OpcionPagoDTO(idOpcionPago,nombreOpcion,medioPago);
			
			double monto = resultSet.getDouble("monto");
			
			TarjetaDTO tarjeta;
			int idTarjeta = resultSet.getInt("idTarjeta");
			if(idTarjeta!=0){
				String nroTarjeta = resultSet.getString("nroTarjeta");
				tarjeta = new TarjetaDTO(idTarjeta,nroTarjeta,null,null,null);
			} else {
				tarjeta = null;
			}
			
			int idEstado = resultSet.getInt("idEstadoVenta");
			String nombreEstado = resultSet.getString("estadoVenta");
			EstadoVentaDTO estadoVenta = new EstadoVentaDTO(idEstado,nombreEstado);
			
			int idSucursal = resultSet.getInt("idSucursal");
			String nombreSucursal = resultSet.getString("nombreSucursal");
			SucursalDTO sucursal = new SucursalDTO(idSucursal,nombreSucursal,null,null,null);
			
			return new VentaDTO(idVenta,fecha,cliente,opcionPago,null,null,monto,tarjeta,estadoVenta,sucursal,0);
	}

	@Override
	public boolean update(VentaDTO venta_a_actualizar) {
		
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		
		try {
			statement = conexion.prepareStatement(updateEstadoVenta);
			statement.setInt(1, venta_a_actualizar.getEstado().getIdEstado());
			statement.setInt(2, venta_a_actualizar.getId());
			if(statement.executeUpdate()>0) {
				conexion.commit();
				isUpdateExitoso = true;
			} else {
				conexion.rollback();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return isUpdateExitoso;
	}

	@Override
	public List<VentaDTO> search(List<Triple<Integer, Integer, String>> filtrosBusqueda) {
		List<VentaDTO> ventas = new ArrayList<VentaDTO>();
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		ResultSet resultSet;
		
		String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(search, arrayCampos, filtrosBusqueda);
		try {
			statement = conexion.prepareStatement(queryBusqueda);
			for(int i=0; i<filtrosBusqueda.size(); i++) {
				statement.setString(i+1, filtrosBusqueda.get(i).getRight());
			}
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				ventas.add(getVentaDTO(resultSet));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ventas;
	}
	
	
}
