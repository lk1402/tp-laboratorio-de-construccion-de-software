package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import dto.AutomovilTallerDTO;
import dto.ClienteDTO;
import dto.EstadoReservaDTO;
import dto.MarcaDTO;
import dto.ModeloDTO;
import dto.MotivoDTO;
import dto.PaisDTO;
import dto.ReservaDTO;
import dto.TipoDocumentoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.ReservaDAO;

public class ReservaDAOSQL implements ReservaDAO {
	private static final String insertReserva = "INSERT INTO reserva(id, fechaEmitida, fechaTurno, horario, cliente, auto, notificada, estado, motivoCancelacion, motivo, tienegarantia, service) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";
	private static final String insertMotivo = "INSERT INTO motivo VALUES (?,?,?,?,?)";
	private static final String insertAutoTaller = "INSERT INTO autosentaller(id, paispatente, patente, modelo, year) VALUES(?, ?, ?, ?, ?)";
	private static final String readReservaIsExist = "SELECT COUNT(id) as cantidad FROM reserva WHERE fechaTurno = ? and horario = ?";
	private static final String cancelar = "UPDATE reserva SET estado = 3, motivoCancelacion = ? WHERE id = ?";
	private static final String readall = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia, reserva.service as service FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id";
//	private static final String readallMatchFechaEmitida = "reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE fechaEmitida like '%' ? '%'";
//	private static final String readallMatchFechaTurno = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE fechaTurno like '%' ? '%'";
//	private static final String readallMatchHorario = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE horario like '%' ? '%'";
//	private static final String readallMatchNombreCliente = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE cliente.nombre like '%' ? '%'";
//	private static final String readallMatchApellidoCliente = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE cliente.apellido like '%' ? '%'";
//	private static final String readallMatchNumDocCliente = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE cliente.numdocumento like '%' ? '%'";
//	private static final String readallMatchMailCliente = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE cliente.mail like '%' ? '%'";
//	private static final String readallMatchTelefonoCliente = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE cliente.telefono like '%' ? '%'";
//	private static final String readallMatchNotificada = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE notificada like '%' ? '%'";
//	private static final String readallMatchEstado = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE estado like '%' ? '%'";
	private static final String updateReserva = "UPDATE reserva SET fechaEmitida = ?, fechaTurno = ?, horario = ?, cliente = ?, notificada = ?, estado = ?, motivoCancelacion = ?, motivo = ?, tienegarantia = ?, service = ? WHERE id = ?";
	private static final String readLastAuto = "SELECT MAX(id) as id From autosentaller";
	private static final String obtenerIdMotivo = "SELECT id FROM motivo WHERE pintura = ? AND mecanica = ? AND electronica_electricidad = ? AND esteticaYAccesorios = ?";
	private static final String readAllReservasNoNotificadas = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia, reserva.service as service FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE notificada = false AND (datediff(fechaTurno, curdate())) <= 7 AND (datediff(fechaTurno, curdate())) > 0";
	private static final String readEmail = "SELECT email, password FROM emailempresa";
	private static final String search = "SELECT reserva.id as idReserva, fechaEmitida, fechaTurno, horario, reserva.cliente, cliente.nombre, cliente.apellido, pais.id as idpais, pais.pais as nombrePais, tipodocumento.id as idTipoDoc, tipodocumento.tipoDocumento as nombreTipoDoc, cliente.numdocumento, cliente.mail, cliente.telefono, reserva.auto as idAuto, autosentaller.paispatente as idPaisPatente, paisPatent.pais as nombrePaisPatente, autosentaller.patente, modeloauto.id as idModelo, modeloauto.modelo as nombreModelo, marcaauto.id as idMarca, marcaauto.marca as nombreMarca, autosentaller.year, notificada, estadoreserva.id as idEstado, estadoreserva.estado as nombreEstado, motivoCancelacion, reserva.motivo as idMotivo, motivo.pintura as motivoPintura, motivo.mecanica as motivoMecanica, motivo.electronica_electricidad as motivoElectronica_Electricidad, motivo.esteticaYAccesorios as motivoEsteticaYAccesorios, reserva.tienegarantia as tieneGarantia, reserva.service AS service FROM reserva join cliente on reserva.cliente = cliente.id join pais on cliente.paisResidencia = pais.id join tipodocumento on cliente.tipoDocumento = tipodocumento.id join autosentaller on reserva.auto = autosentaller.id join pais as paisPatent on autosentaller.paispatente = paisPatent.id join modeloauto on autosentaller.modelo = modeloauto.id join marcaauto on modeloauto.marca = marcaauto.id join estadoreserva on reserva.estado = estadoreserva.id join motivo on reserva.motivo = motivo.id WHERE ";
	private static final String[] arrayCampos = {"cliente.nombre","cliente.apellido","cliente.numdocumento","cliente.mail","cliente.telefono","estadoreserva.estado","reserva.fechaTurno","autosentaller.patente"};
	private static final String updateReservasVencidas  = "UPDATE reserva SET estado = 3, motivoCancelacion = 'El cliente no se presentó'  WHERE estado = 1 AND fechaTurno < curdate()";
	//crea un cliente si no existe
	public boolean insert(ReservaDTO reserva)
	{
		PreparedStatement statementReserva;//sentencia insert reserva
		PreparedStatement statementIsExistReserva;//sentencia consulta si reserva existe
		PreparedStatement statementAuto;
		PreparedStatement statementLastAuto;
		PreparedStatement statementLastMotivo;
		PreparedStatement statementMotivo;
		
		ResultSet resultSetIsExistReserva;//resultado de si existe la reserva
		ResultSet resultSetLastAuto;
		ResultSet resultSetIdMotivo;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try{		
			
			MotivoDTO motivo = reserva.getMotivo();
			statementMotivo = conexion.prepareStatement(obtenerIdMotivo);
			statementMotivo.setBoolean(1, motivo.isPintura());
			statementMotivo.setBoolean(2, motivo.isMecanica());
			statementMotivo.setBoolean(3, motivo.isElectronica_electricidad());
			statementMotivo.setBoolean(4, motivo.isEsteticaYAccesorios());
			
			resultSetIdMotivo = statementMotivo.executeQuery();
			if(resultSetIdMotivo.next()) {
			int idMotivo = resultSetIdMotivo.getInt("id");								

			
					if(reserva.getAuto().getId()==0){
						
						statementAuto = conexion.prepareStatement(insertAutoTaller);
						statementAuto.setInt(1, reserva.getAuto().getId());
						statementAuto.setInt(2, reserva.getAuto().getPaisPatente().getIdPais());
						statementAuto.setString(3, reserva.getAuto().getPatente());
						statementAuto.setInt(4, reserva.getAuto().getModelo().getId());
						statementAuto.setString(5, reserva.getAuto().getYear());
						
						if(statementAuto.executeUpdate() > 0){
							
							int idAuto = 0;
							statementLastAuto = conexion.prepareStatement(readLastAuto);
							resultSetLastAuto = statementLastAuto.executeQuery();
							if(resultSetLastAuto.next()){
								idAuto = resultSetLastAuto.getInt("id");
								
									
								
								
								statementReserva = conexion.prepareStatement(insertReserva);
								
//								System.out.println(reserva.getIdReserva()+"\n"+reserva.getFechaEmitida());
//								System.out.println(reserva.getFechaTurno()+"\n"+reserva.getHorario());
//								System.out.println(reserva.getCliente().getIdCliente()+"\n"+reserva.getAuto().getId());
//								System.out.println( reserva.isNotificada()+"\n"+reserva.getEstado().getIdEstado());
//								System.out.println( "id auto"+"\n"+idAuto);
								statementReserva.setInt(1, reserva.getIdReserva());
								statementReserva.setString(2, reserva.getFechaEmitida());
								statementReserva.setString(3, reserva.getFechaTurno());
								statementReserva.setString(4, reserva.getHorario());
								statementReserva.setInt(5, reserva.getCliente().getIdCliente());
								statementReserva.setInt(6, idAuto);
								statementReserva.setBoolean(7, reserva.isNotificada());
								statementReserva.setInt(8, reserva.getEstado().getIdEstado());
								statementReserva.setString(9, reserva.getMotivoCancelacion());
								statementReserva.setInt(10, idMotivo);
								statementReserva.setBoolean(11, reserva.isTieneGarantia());
								statementReserva.setString(12, reserva.getService());
								if(statementReserva.executeUpdate() > 0){
									conexion.commit();
									isInsertExitoso = true;
								}else conexion.rollback();
							
								
							} else {
								conexion.rollback();
							}
						}
					}else{
						statementReserva = conexion.prepareStatement(insertReserva);
						statementReserva.setInt(1, reserva.getIdReserva());
						statementReserva.setString(2, reserva.getFechaEmitida());
						statementReserva.setString(3, reserva.getFechaTurno());
						statementReserva.setString(4, reserva.getHorario());
						statementReserva.setInt(5, reserva.getCliente().getIdCliente());
						statementReserva.setInt(6, reserva.getAuto().getId());
						statementReserva.setBoolean(7, reserva.isNotificada());
						statementReserva.setInt(8, reserva.getEstado().getIdEstado());
						statementReserva.setString(9, reserva.getMotivoCancelacion());
						statementReserva.setInt(10, idMotivo);
						statementReserva.setBoolean(11, reserva.isTieneGarantia());
						statementReserva.setString(12, reserva.getService());
						if(statementReserva.executeUpdate() > 0){
							conexion.commit();
							isInsertExitoso = true;
						}else conexion.rollback();
					}
			}
			
		}catch (SQLException e){
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}
	
	//cancela una reserva si existe
	public boolean cancelar(ReservaDTO reserva_a_cancelar, String motivoCancelacion)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean iscancelarExitoso = false;
		try 
		{
			statement = conexion.prepareStatement(cancelar);
			statement.setString(1, motivoCancelacion);
			statement.setInt(2, reserva_a_cancelar.getIdReserva());
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				iscancelarExitoso = true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return iscancelarExitoso;
	}
	
	//devuelve una lista con todos los reserva
	public List<ReservaDTO> readAll(){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ReservaDTO> reservas = new ArrayList<ReservaDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				reservas.add(getReservaDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return reservas;
	}
	
	//devuelve una lista con todos las reserva que coincidan con el dato en el campo que fueron pasados como parametros
//	public List<ReservaDTO> readAllMatchDatReserve(String datOfReserve, String campo){
//		PreparedStatement statement;
//		ResultSet resultSet; //Guarda el resultado de la query
//		ArrayList<ReservaDTO> reservas = new ArrayList<ReservaDTO>();
//		Conexion conexion = Conexion.getConexion();
//		try{
//			statement = null;
//			if(campo.equals("fechaEmitida")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchFechaEmitida);
//			}else if(campo.equals("fechaTurno")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchFechaTurno);
//			}else if(campo.equals("horario")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchHorario);
//			}else if(campo.equals("nombreCliente")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchNombreCliente);
//			}else if(campo.equals("apellidoCliente")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchApellidoCliente);
//			}else if(campo.equals("numDocCliente")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchNumDocCliente);
//			}else if(campo.equals("mailCliente")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchMailCliente);
//			}else if(campo.equals("telefonoCliente")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchTelefonoCliente);
//			}else if(campo.equals("notificada")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchNotificada);
//			}else if(campo.equals("estado")){
//				statement = conexion.getSQLConexion().prepareStatement(readallMatchEstado);
//			}
//			
//			statement.setString(1, datOfReserve);
//			resultSet = statement.executeQuery();
//			while(resultSet.next()){
//				reservas.add(getReservaDTO(resultSet));
//			}
//		}catch (SQLException e){
//			e.printStackTrace();
//		}
//		return reservas;
//	} 
	
	public List<ReservaDTO> readAllReservasNoNotificadas(){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<ReservaDTO> reservas = new ArrayList<ReservaDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readAllReservasNoNotificadas);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				reservas.add(getReservaDTO(resultSet));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return reservas;
	}
	
	public Pair<String, String> readEmail(){
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Pair<String, String> email = null;
		Conexion conexion = Conexion.getConexion();
		try{
			statement = conexion.getSQLConexion().prepareStatement(readEmail);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				email = new ImmutablePair<String, String>(resultSet.getString("email"), resultSet.getString("password"));
			}
		}catch (SQLException e){
			e.printStackTrace();
		}
		return email;
	}
	
	//arma un objeto reservaDTO
	private ReservaDTO getReservaDTO(ResultSet resultSet) throws SQLException
	{
		int id = resultSet.getInt("idReserva");
		String fechaEmitida = resultSet.getString("fechaEmitida");
		String fechaTurno = resultSet.getString("fechaTurno");
		String horario = resultSet.getString("horario");
		int idcliente = resultSet.getInt("cliente");
		String nombreCliente = resultSet.getString("nombre");
		String apellidoCliente = resultSet.getString("apellido");
		int idPaisResidencia = resultSet.getInt("idpais");
		String nombrePaisResidencia = resultSet.getString("nombrePais");
		PaisDTO paisResidencia = new PaisDTO(idPaisResidencia, nombrePaisResidencia);
		int idTipoDoc = resultSet.getInt("idTipoDoc");
		String nombreTipoDoc = resultSet.getString("nombreTipoDoc");
		TipoDocumentoDTO tipoDoc = new TipoDocumentoDTO(idTipoDoc, nombreTipoDoc);
		String numDocCliente =resultSet.getString("numdocumento");
		String mailCliente =resultSet.getString("mail");
		String telefonoCliente =resultSet.getString("telefono");
		ClienteDTO cliente = new ClienteDTO(idcliente, nombreCliente, apellidoCliente, paisResidencia, tipoDoc, numDocCliente, mailCliente, telefonoCliente);
		int idAuto = resultSet.getInt("idAuto");
		int idPaisPatente = resultSet.getInt("idPaisPatente");
		String nombrePaisPatente = resultSet.getString("nombrePaisPatente");
		PaisDTO paisPatente = new PaisDTO(idPaisPatente, nombrePaisPatente);
		String patente = resultSet.getString("patente");
		int idMarca = resultSet.getInt("idMarca");
		String nombreMarca = resultSet.getString("nombreMarca");
		MarcaDTO marca = new MarcaDTO(idMarca, nombreMarca);
		int idModelo = resultSet.getInt("idModelo");
		String nombreModelo = resultSet.getString("nombreModelo");
		ModeloDTO modelo = new ModeloDTO(idModelo, nombreModelo, marca);
		String year = resultSet.getString("year");
		AutomovilTallerDTO auto = new AutomovilTallerDTO(idAuto, paisPatente, patente, modelo, year);
		boolean notificada =resultSet.getBoolean("notificada");
		int idEstado = resultSet.getInt("idEstado");
		String nombreEstado = resultSet.getString("nombreEstado");
		EstadoReservaDTO estadoReserva = new EstadoReservaDTO(idEstado, nombreEstado);		
		String motivoCancelacion =resultSet.getString("motivoCancelacion");
		MotivoDTO motivo = new MotivoDTO(resultSet.getInt("idMotivo"),resultSet.getBoolean("motivoPintura"), resultSet.getBoolean("motivoMecanica"), resultSet.getBoolean("motivoElectronica_Electricidad"), resultSet.getBoolean("motivoEsteticaYAccesorios"));
		String service = resultSet.getString("service");
		boolean tieneGarantia = resultSet.getBoolean("tieneGarantia");
		
		
		return new ReservaDTO(id, fechaEmitida, fechaTurno, horario, cliente, auto, notificada, estadoReserva, motivoCancelacion, motivo, tieneGarantia, service);
	}
	
	//modifica un reserva
	public boolean update(ReservaDTO reservaModificada) {
		PreparedStatement statementReserva;//sentencia update reserva
		PreparedStatement statementMotivo;
	
		ResultSet resultsetIdMotivo;
		
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try{		
			statementMotivo = conexion.prepareStatement(obtenerIdMotivo);
			statementMotivo.setBoolean(1, reservaModificada.getMotivo().isPintura());
			statementMotivo.setBoolean(2, reservaModificada.getMotivo().isMecanica());
			statementMotivo.setBoolean(3, reservaModificada.getMotivo().isElectronica_electricidad());
			statementMotivo.setBoolean(4, reservaModificada.getMotivo().isEsteticaYAccesorios());
			
			resultsetIdMotivo = statementMotivo.executeQuery();
			if(resultsetIdMotivo.next()) {
			int idMotivo = resultsetIdMotivo.getInt("id");
				
			statementReserva = conexion.prepareStatement(updateReserva);
			statementReserva.setString(1, reservaModificada.getFechaEmitida());
			statementReserva.setString(2, reservaModificada.getFechaTurno());
			statementReserva.setString(3, reservaModificada.getHorario());
			statementReserva.setInt(4, reservaModificada.getCliente().getIdCliente());
			statementReserva.setBoolean(5, reservaModificada.isNotificada());
			statementReserva.setInt(6, reservaModificada.getEstado().getIdEstado());
			statementReserva.setString(7, reservaModificada.getMotivoCancelacion());
			statementReserva.setInt(8, idMotivo);
			statementReserva.setBoolean(9, reservaModificada.isTieneGarantia());
			statementReserva.setString(10, reservaModificada.getService());
			statementReserva.setInt(11, reservaModificada.getIdReserva());
			

			
//			boolean a = statementReserva.executeUpdate() > 0;
//			boolean b = statementReserva.executeUpdate() > 0;
			if(statementReserva.executeUpdate() > 0){
				conexion.commit();
				isUpdateExitoso = true;
			}else conexion.rollback();
		}
		}catch(SQLException e) {
			e.printStackTrace();
			try{
				conexion.rollback();
			}catch(SQLException e1){
				e1.printStackTrace();
			}
		}
		
		return isUpdateExitoso;
	}
	
	public List<ReservaDTO> search(List<Triple<Integer,Integer,String>> filtrosBusqueda){
		PreparedStatement statement;
		ResultSet resultSet;
		List<ReservaDTO> reservas = new ArrayList<ReservaDTO>();
		Conexion conexion = Conexion.getConexion();
		try{
			String queryBusqueda = GeneradorBusquedaSQL.generarQueryBusqueda(this.getSearch(),this.getArraycampos(),filtrosBusqueda);
			if(queryBusqueda!=null) {
			statement = conexion.getSQLConexion().prepareStatement(queryBusqueda);
			for(int i=0;i<filtrosBusqueda.size();i++) {
				statement.setString(i+1, filtrosBusqueda.get(i).getRight());
			}
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				reservas.add(getReservaDTO(resultSet));
			}
			}
		}catch (SQLException e){
			e.printStackTrace();
		
		}
		
		return reservas;
	}

	public static String getSearch() {
		return search;
	}

	public static String[] getArraycampos() {
		return arrayCampos;
	}

	@Override
	public boolean updateVencidas() {
		boolean isUpdateExitoso = false;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try {
			PreparedStatement statement = conexion.prepareStatement(updateReservasVencidas);
			if(statement.executeUpdate()>0) {
				conexion.commit();
				isUpdateExitoso = true;
			}else conexion.rollback();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isUpdateExitoso;
	}
	
}
