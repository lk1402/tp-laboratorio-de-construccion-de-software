package persistencia.dao.interfaz;

import java.util.List;

import dto.MedioPagoDTO;

public interface MedioPagoDAO {
	
	public List<MedioPagoDTO> readAll();
}
