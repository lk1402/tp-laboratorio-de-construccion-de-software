package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import dto.AutoparteDTO;
import dto.ModeloDTO;
import dto.StockAutomovilDTO;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.Dimension;

public class VentanaAlarmaStock extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private static VentanaAlarmaStock INSTANCE;

	private JTable tablaProductosFaltantes;
	private DefaultTableModel modelProductosFaltantes;
	private String[] nombreColumnas = {"Tipo de Producto", "Modelo","Color","Año","Cantidad Puertas","Sucursal","Stock Minimo", "Stock Actual", "Faltante"};

	private int modo;//0 -> se entro directamente desde abm o al iniciar, 1 -> se activos desde otra abm
	private JPanel panelsuperior;
	private JPanel panelinferior;
	private JButton btnReporteAutos;
	private JButton btnReporteAutopartes;
	private JButton btnVolver;
	private JLabel lblTablaStockFaltante;
	private boolean tablaVacia;
	
	public VentanaAlarmaStock() {
		setMinimumSize(new Dimension(387, 159));
		setBounds(new Rectangle(0, 0, 800, 450));
		setTitle("Alarma de Falta de Stock");
		setLocationRelativeTo(null);
		ImageIcon imagen = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/logoSistema.png"));
		imagen = new ImageIcon(imagen.getImage().getScaledInstance(30, 30, Image.SCALE_DEFAULT));
		setIconImage(imagen.getImage());
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane spAutopartesFaltantes = new JScrollPane();
		panel.add(spAutopartesFaltantes, BorderLayout.CENTER);
		
		modelProductosFaltantes = new DefaultTableModel(null,nombreColumnas);
		tablaProductosFaltantes = new JTable(modelProductosFaltantes);
		tablaProductosFaltantes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);		
		tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		spAutopartesFaltantes.setViewportView(tablaProductosFaltantes);
		
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent arg0) {
				if(getWidth()<1142){
					if(tablaVacia)tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
					else tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				}
				else tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			}
		});
		
		ImageIcon imagenflecha = new ImageIcon(VentanaPrincipal.class.getResource("/iconos/flecha_izq.png"));
		imagenflecha = new ImageIcon(imagenflecha.getImage().getScaledInstance(30, 20, Image.SCALE_DEFAULT));
		
		panelsuperior = new JPanel();
		panel.add(panelsuperior, BorderLayout.NORTH);
		
		lblTablaStockFaltante = new JLabel("Stock Faltante");
		lblTablaStockFaltante.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panelsuperior.add(lblTablaStockFaltante);
		
		panelinferior = new JPanel();
		panel.add(panelinferior, BorderLayout.SOUTH);
		
		btnReporteAutos = new JButton("Generar Reporte Autos");
		panelinferior.add(btnReporteAutos);
		
		btnReporteAutopartes = new JButton("Generar Reporte Autopartes\r\n");
		panelinferior.add(btnReporteAutopartes);
		
		btnVolver = new JButton("Volver");
		panelinferior.add(btnVolver);
		


	}
	
	public static VentanaAlarmaStock getInstance()
	{
		if(INSTANCE == null)
		{
			INSTANCE = new VentanaAlarmaStock(); 	
			return new VentanaAlarmaStock();
		}
		else
			return INSTANCE;
	}
	
	public void llenarAutos(List<StockAutomovilDTO> listaAutos,List<AutoparteDTO>listaAutopartes){
		if(listaAutos.size()>0||listaAutopartes.size()>0){
			tablaVacia = false;
			if(getWidth()<1142)tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			else tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}else{
			tablaVacia = true;
			tablaProductosFaltantes.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		}
		
		this.modelProductosFaltantes = new DefaultTableModel(null,nombreColumnas);
		this.tablaProductosFaltantes.setModel(modelProductosFaltantes);
		for (StockAutomovilDTO a : listaAutos)
		{
			String tipoProducto = a.getTipoString();
			ModeloDTO modelo = a.getModelo();
			String modeloCompleto = modelo.getMarca().getNombre() + " " + modelo.getModelo();
			String color = a.getColor();
			String year = a.getYear();
			String cantidadPuertas = a.getCantidadPuertas();
			String sucursal = a.getSucursal().getNombre();
			int stockMinimo = a.getStockMinimo();
			int stockActual = a.getStock();
			String txtStockMinimo = Integer.toString(stockMinimo);
			String txtStockActual = Integer.toString(stockActual);
			String faltante = Integer.toString(stockMinimo - stockActual);
			Object[] fila = {tipoProducto,modeloCompleto,color,year,cantidadPuertas,sucursal,txtStockMinimo,txtStockActual,faltante};
			this.getModelProductosFaltantes().addRow(fila);
			this.redefinirDimensionTabla();
		}
		for (AutoparteDTO ap: listaAutopartes) {
			String tipoProducto = ap.getTipoString();
			ModeloDTO modelo = ap.getModelo();
			String modeloCompleto = modelo.getMarca().getNombre() + " " + modelo.getModelo();
			String color = null;
			String year = ap.getYear();
			String cantidadPuertas = null;
			String sucursal = ap.getSucursal().getNombre();
			int stockMinimo = ap.getStockMinimo();
			int stockActual = ap.getStock();
			String txtStockMinimo = Integer.toString(stockMinimo);
			String txtStockActual = Integer.toString(stockActual);
			String faltante = Integer.toString(stockMinimo - stockActual);
			Object[] fila = {tipoProducto,modeloCompleto,color,year,cantidadPuertas,sucursal,txtStockMinimo,txtStockActual,faltante};
			this.getModelProductosFaltantes().addRow(fila);
			this.redefinirDimensionTabla();
		}
	}
	
	private void redefinirDimensionTabla(){
		JTable tabla = this.getTablaProductosFaltantes();
        for (int i = 0; i < tabla.getColumnCount(); i++) {
            DefaultTableColumnModel colModel = (DefaultTableColumnModel) tabla.getColumnModel();
            TableColumn col = colModel.getColumn(i);
            int width = 100;
            
            TableCellRenderer renderer = col.getHeaderRenderer();
            for (int r = 0; r < tabla.getRowCount(); r++) {
            	renderer = tabla.getCellRenderer(r, i);
                Component comp = renderer.getTableCellRendererComponent(tabla, tabla.getValueAt(r, i),false, false, r, i);
                width = Math.max(width, comp.getPreferredSize().width);
            }
            col.setPreferredWidth(width + 25);
        }
    }
	
	public void mostrarVentana() {
		this.setVisible(true);
	}

	public JTable getTablaProductosFaltantes() {
		return tablaProductosFaltantes;
	}

	public DefaultTableModel getModelProductosFaltantes() {
		return modelProductosFaltantes;
	}
	
	public JButton getBtnReporteAutos() {
		return btnReporteAutos;
	}

	public JButton getBtnReporteAutopartes() {
		return btnReporteAutopartes;
	}

	public JButton getBtnVolver() {
		return btnVolver;
	}

	public int getModo() {
		return modo;
	}

	public void setModo(int modo) {
		this.modo = modo;
	}
}

