package modelo;

import java.util.List;

import dto.StockAutomovilDTO;
import dto.AutomovilTallerDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.AutomovilTallerDAO;

public class AutomovilTaller {
	
	private AutomovilTallerDAO automovil;
	
	public AutomovilTaller (DAOAbstractFactory metodo_persistencia)
	{
		this.automovil = metodo_persistencia.createAutomovilTallerDAO();
	}
	
	public AutomovilTallerDTO obtenerAutomovilIfExist(AutomovilTallerDTO auto)
	{
		return this.automovil.readAllIfExist(auto);		
	}
	
}
