package modelo;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import dto.StockAutomovilDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.AutomovilDAO;

public class Automovil {
	
	private AutomovilDAO automovil;
	
	public Automovil (DAOAbstractFactory metodo_persistencia)
	{
		this.automovil = metodo_persistencia.createAutomovilDAO();
	}
	
	public List<StockAutomovilDTO> obtenerAutomoviles(int modelo)
	{
		return this.automovil.readConModelos(modelo);		
	}
	
	public List<StockAutomovilDTO> obtenerAutomovilesPorSucursal(int modelo, int idSucursal)
	{
		return this.automovil.readConModelosPorSucursal(modelo, idSucursal);		
	}
	
//	public List<StockAutomovilDTO> obtenerAutomoviles()
//	{
//		return this.automovil.readAll();		
//	}

	
	public boolean agregarAutomovil(StockAutomovilDTO nuevoAutomovil) {
		return this.automovil.insert(nuevoAutomovil);
	}
	
	public boolean editarAutomovil(StockAutomovilDTO automovilModificado) {
		return this.automovil.update(automovilModificado);
	}
	
	public boolean borrarAutomovil(StockAutomovilDTO automovil_a_eliminar) 
	{
		return this.automovil.delete(automovil_a_eliminar);
	}
	
	public List<StockAutomovilDTO> automovilesFaltaStock(){
		return this.automovil.readAllFaltaStock();
	}
	
	public List<StockAutomovilDTO> filtrarAutomoviles(List<Triple<Integer,Integer,String>> filtrosBusqueda) {
		return this.automovil.search(filtrosBusqueda);
	}
	
	
}
